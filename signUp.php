<!DOCTYPE html> 
<html lang='vn'> 
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head> 
<style>
    .error{
        color: red;
    }
</style>
<title>Sign Up Form</title>
<body>
    <fieldset style='width: 500px; height: 400px; border:#ADD8E6 solid; margin:0 auto'>
    <?php
        $name = $gender = $selectGroup = $dateOfBirth = $address= "";
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty(inputHandling($_POST["name"]))){
                echo "<div class='error'>Hãy nhập tên!</div>";
            }
            if (empty($_POST["gender"])) {
                echo "<div class='error'>Hãy chọn giới tính!</div>";
            }
            if(empty(inputHandling($_POST["selectGroup"]))){
                echo "<div class='error'>Hãy chọn phân khoa!</div>";
            }

            if(empty(inputHandling($_POST["dateOfBirth"]))){
                echo "<div class='error'>Hãy nhập ngày sinh!</div>";
            }
            elseif (!validationDate($_POST["dateOfBirth"])) {
                echo "<div class='error'>Hãy nhập ngày sinh đúng định dạng!</div>";
            }
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validationDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }

        }
    ?>
    <form style='margin: 20px 50px 0 35px' method="post">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '33px'>
                <td width = 10% style = 'background-color: #5db45d; 
                vertical-align: top; text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white;'>Họ và tên<span class="error"> * </span></label>
                </td>
                <td width = 10% >
                    <input type='text' name = "name" style = 'line-height: 32px; 
                    border: 1.5px solid #2d6d98; 
                    width: 100%;'>
                </td>
            </tr>
            <tr height = '35px'>
                <td width = 10% style = 'background-color: #5db45d; vertical-align: top; text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white;'>Giới tính<span class="error"> * </span></label>
                </td>
                <td width = 10% >
                <?php
                    $gender = array(0, 1);
                    for( $x=0; $x<=1; $x++) {
                        if($x == 0){
                            echo "<label for='male' class='gender'>
                                <input type='radio' class='gender' name='gender'> Nam 
                            </label>";
                        }
                        elseif($x == 1){
                            echo "<label for='female' class='gender'>
                                <input type='radio' class='gender' name='gender'> Nữ 
                            </label>";
                        }
                    }
                ?>  

                </td>
            </tr>
            <tr height = '33px'>
                <td style = 'background-color: #5db45d; vertical-align: top; text-align: center; padding: 5px 5px;border: 1.5px solid #2E8BC0'>
                    <label style='color: white;'>Phân Khoa<span class="error"> * </span></label>
                </td>
                <td height = '35px'>
                    <select name='selectGroup' style = 'border: 1.5px solid #2d6d98 ;width: 80%; height: 32px'>
                        <?php
                            $selectGroupArr = array(" ", "MAT", "KDL");
                            foreach($selectGroupArr as $f){
                                if($f == " "){
                                    echo "<option value=''>
                                        
                                    </option>";
                                }
                                elseif($f == "MAT"){
                                    echo "<option value='MAT'>
                                        Khoa học máy tính
                                    </option>";
                                }
                                elseif($f == "KDL"){
                                    echo "<option value='KDL'>
                                        Khoa học vật liệu
                                    </option>";
                                }
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr height = '33px'>
                <td style = 'background-color: #5db45d; vertical-align: top; text-align: center; padding: 5px 5px; border: 1.5px solid #2E8BC0'>
                    <label style='color: white;'>Ngày sinh<span class="error"> * </span></label>
                </td>
                <td height = '33px'>
                    <input type='date' name="dateOfBirth" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border: 1.5px solid #2d6d98; color: grey'>
                </td>
            </tr>

            <tr height = '33px'>
                <td style = 'background-color: #5db45d; vertical-align: top; text-align: center; padding: 5px 5px;border: 1.5px solid #2E8BC0'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '33px'>
                    <input type='text' name="address" style = 'line-height: 32px; 
                    border: 1.5px solid #2d6d98; 
                    width: 100%;'> 
                </td>
            </tr>

        </table>
        <input type='submit' value='Đăng ký' id='submit' 
        style='background-color: #5db45d; 
        border-radius: 10px; 
        width: 35%; height: 43px; 
        border: 1.5px solid #2e8bc0;
        margin: 20px 130px; 
        color: white'/>
    </form>
</fieldset>
</body>
</html>


